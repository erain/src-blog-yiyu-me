---

title: 别了Amazon——一个工程师在Amazon两年的感悟

date: 2017-01-07 08:00:00

tags: 声

---

![amazon-logo](https://farm1.staticflickr.com/300/31801384200_64cddfd72d_c.jpg)

我于2014年11月3日正式入职Amazon在温哥华的办公司，到2016年12月2日离职，历时整整2年1个月。由于已经有了下一个职业目标，所以我在提出辞职的时候比较轻松。哪知真到了离职的这天，我却赫然发现心中隐约的依依不舍：不但是对优秀的同事，甚至还有对Amazonian(亚马逊人)这个身份。

我离职前恰逢一件意外发生：一个西雅图Amazon总部的工程师因为被老板放到了PIP中，愤而跳楼了(注：PIP即Performance Improvement Plan，大公司用于开除人的工具)。这让亚马逊的“特别”的企业文化继去年纽约时报的批评之后又一次走上了风口浪尖。也不断朋友问我这是不是我离开亚马逊的原因，对此我的回答很简单：我决定离开的原因是因为我相信下一个机会更适合我。

抛开企业文化这个有争议的话题不说，亚马逊可以从一个小小的卖书网站成长为全球范围内电商的霸主，技术上在云计算领域也是绝对的第一名，我们不妨来看看它积极地独一无二的一面。于是，我想回顾一下个人在亚马逊学习到的东西和感悟，作为对于过去两年的一个总结。这篇文章并非技术帖，更多是我个人成长更多的非技术领域。

### Ownership(主人翁精神)

在江湖上Amazon有著名的[Leadership Principle](https://www.amazon.jobs/principles)，其中Ownership排在第二——仅次于第一名的Customer Obsession(客户至上)。而在公司里赞扬或者批评一个员工也经常以这条为标准。

然而，我想诠释的这条和大多数人对于主人翁精神的理解比如“你要把公司的事情当做自己的事情”之列的有着非常大的不同。那是公司希望说给员工的。我想说的Onwership，可以简洁的总结成一句话：你要对于你自己职业生涯的付全责。

我在Amazon的良师益友Desmond同学对于这句话有着一段非常精彩的诠释。大致意思是：我从来不觉得我是给公司在打工，我为我自己工作。当我的目标和公司一致的时候我在公司内用Startup的心态来做事情，当目标不一致的时候再寻求改变。

因此，如果你有Ownership，你选择一份工作无论是在大公司还是小公司还是自己去创业，其实没有那么大的不同——只要你为你的职业生涯(career)而不是工作(job)而工作。

### Action Item (行动事项)

江湖上另外一个传言就是Amazon开会不用PPT，而是会议主持人给大家发一个6页纸的文档(6 pages)，大家猛读10分钟后开始会议内容。

由于我在公司内级别不高，所以虽然以上会议形式常见但是6 pages并不算常见。但让我收益很深的是Amazon开会、讨论的另一个鲜明的特点：Action Items(行动事项)。

每当开会大家激烈的讨论完一个话题之后，可能会有几个可能的解决方案，也可能会有更多的问题，这个时候会议室里总会有人说出这么一句话：“So what is the action item for this?(所以，关于这件事的行动事项是什么？)”

在Amazon，无论是开完一个会后的会议记录，还是一个ticket里面的回复，里面都需要明确的列出行动事项。一个会议如果没有行动事项，就等于没开。行动事项不仅仅只是列出要做的事情，更要列出谁来做这件事，此事当前的进展，以及预期完成的时间(ETA)。不仅如此，这份行动事项会在列在一个各方都能看到的文档里实时更新。

我想，行动事项的方法之所以有效，是因为它是一个非常有效的思维工具，把一个要达成的目标分离成了决策和行动两方面：这迫使我们在决策的时候就要考虑如何把大目标分成一个又一个可执行(actionable)的小目标，又在行动时让我们不再思前想后只需要按照原定计划一条一条执行下局即可。

### 吃掉大象：推动项目的能力

我之前的经理经常挂在嘴边的一句话我很喜欢：

> How to eat an elephant? One bite at a time. :D
> 
> 怎么吃掉一个大象？一口一口吃咯。

我之所以喜欢这句话，是因为它首先抛出了一个看似很难以完成的问题，然后又给出了既让人吃惊又合情合理的回答。这和项目管理很像：一个无论是多大的项目，只要自顶向下一层一层划分下去，总能找到着手点开始做的。

具体地说，任何项目A，都可以分成小的子项目A1,A2,A3…，然后这些子项目比如A1又可以分成更小的子项目A11,A12,A13…，如此细分下去我们总可以到达一个层面：在这层的所有子项目都是可执行的(actionable)，于是我们就得到了我们的行动列表了(:D)。

另一维度就是时间，在Amazon经常提到的一句话是thinking backwards(从后往前考虑)。这句话的意思是，假设我们需要2017年年末上线一个项目，那么12月需要完成什么，11月完成什么，如此这般往前倒推回去到今天。这样一来，行动事项就和项目的规划(schedule)一对一的匹配上了。

Amazon公司内部由一个工程师来发起和推动一个项目的实例屡见不鲜，有一个小故事是：早期的Amazon首页上并没有商品推荐功能，有一个工程师想推动此事但是不被批准，于是他就偷偷地实现了这个功能并且上线到10%的流量中，最终推荐功能的Amazon页面购买率远高于没有推荐功能的，项目也获得批准。

现阶段现实生活中的Amazon（或者其他同样档次的大公司），这样的事情几乎不可能发生。而在亚马逊内部，资深的工程师和缺乏经验的的工程师的区别就在于推动项目的能力：但是如果一个工程师提出一个项目，里面清晰地描绘了“如何吃掉一个大象”，那么他的项目获得支持以及获得成功的可能性将大大增强。

### 后记

这篇文章本来在离职当天就想写，结果一拖就拖了这么久。写之前思绪万千，提起笔来挂一漏万。想想还是有些汗颜，觉得对不起起了这么大的一个题目……但想到此文只是作为自己的一个总结，也就释然了。

文中没有提到一点具体的技术，原因很简单：这个时代技术已经不是秘密，只要有心学习网络上的资料太多了。一个特别推荐的资源是 Werner Vogels的博客[All Things Distributed](https://www.allthingsdistributed.com)，他是Amazon的CTO，里面有一系列文章叫做Back-to-Basics Weekend Reading里面包含了很多数据库以及分布式系统中的经典论文，很有意思。

最后，祝福我Amazon的朋友们发展得越来越好。我虽然不再是Amazon的员工，但是还会是Amazon的客户的。 :D

![amazon-last-day](https://farm1.staticflickr.com/771/32058797671_66e63bd957_c.jpg)


