---
title: Hello World

date: 2016-12-31 20:35:01

tags:
 
  - 声
  - 色
  - 犬
  - 马

---

距离上次写博客可能有5年了吧……当时还是个刚从学校毕业充满憧憬的学生，现在已经变成了一个愤世嫉俗的犬儒主义大叔了……

但是，文章还是要写的，万一有人看呢？新的博客地址在 [blog.yiyu.me](http://blog.yiyu.me) ，直接host在GitHub上，请多关照。

最后按照历史惯例第一篇博客来一段Hello World：

``` bash
$ python -c "print 'hello world'"
```
