---
title: VMWare虚拟机设置
date: 2019-03-31 08:10:14
tags: 犬
---

因为不想要有多个不同的开发环境，所以这么多年我的主力开发环境一直是Mac。但是有很多情况下又必须使用Linux，比如一些Linux Only的软件的开发比如[Containerd](https://github.com/containerd/containerd)，或者需要搭建一个K8s集群。VMWare虚拟机在这种情况下就是很方便的选择，这篇博客就记录一下一些常用的VMWare虚拟机设置。假定Host是macOS，VMWare版本是VMWare Fusion Pro。

### 静态IP设置

VMWare默认是通过DHCP动态分配IP，这就造成了假设如果我们想要假设一个K8s集群，Master和Nodes没办法得到一个固定的IP进行通信。

想要设定固定IP也很容易，只要修改一个虚拟网卡的DHCP设置就行了。这个方法来源于[Set a Static IP Address in VMware Fusion 7](https://willwarren.com/2015/04/02/set-static-ip-address-in-vmware-fusion-7/)

首先获得一个虚拟机网卡的MAC地址，如下图所示：

![VMWare-VM-Mac-Address](https://willwarren.com/images/2015/04/Screenshot%202015-04-03%2010.55.45.png)

然后，在VMWare的默认`vmnet8`网卡的DHCP设置里增加针对这个MAC地址的静态IP设置。

``` shell
# /Library/Preferences/VMware Fusion/vmnet8/dhcpd.conf

host vm0 {
	hardware ethernet 00:0C:29:72:88:46;
	fixed-address  172.16.76.130;  				# IPs available are 172.16.76.128~254
}
```

重启虚拟机和VMWare即可。

### Linux与Host共享目录

如果是有图形界面的Linux，装上VMWare Tools之后就可以看到贡献的目录了。

对于没有图形界面的Linux：

1. [命令行安装VMWare Tools](https://docs.vmware.com/en/VMware-Workstation-Pro/15.0/com.vmware.ws.using.doc/GUID-08BB9465-D40A-4E16-9E15-8C016CC8166F.html)。
2. 在VMWare中共享目录
   ![vmware-fusion-share-folder](https://storage.googleapis.com/11-technology-static/Screen%20Shot%202019-03-31%20at%208.56.44%20AM.png)

3. 在`/etc/fstab`中增加相应的mount point:

``` shell
 .host:/src /home/yiyu/go/src fuse.vmhgfs-fuse allow_other,uid=1000,gid=1000,auto_unmount,defaults 0 0
```

