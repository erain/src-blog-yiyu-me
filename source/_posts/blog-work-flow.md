---

title: Hexo博客Workflow

date: 2018-01-28 20:32:46

tags: 犬

---

定下了2018年的新年愿望为“多写博客”后，上一次更新还是一年以前…连[hexo](https://hexo.io)如何使用都已经忘了。幸好之前的设置代码都还保存在Bitbucket里面，花了一点时间才把才把博客在本地架起来。

这篇博客记录了这个`hexo-based`博客的workflow。

### 安装hexo，同步repo

安装Hexo的方法很简单，首先要保证系统中安装了[npm](https://www.npmjs.com)。然后：

``` bash
npm install hexo-cli -g
```

我用的是theme是修改过的[next](https://github.com/theme-next/hexo-theme-next)。如果需要修改：

``` bash
git clone git@bitbucket.org:erain/hexo-theme-next-erain.git
```

博客代码中已经把theme加成了[git submodule](https://github.com/blog/2104-working-with-submodules)。因此，如果不需要修改theme的时候，只需要：

``` bash
git clone git@bitbucket.org:erain/src-blog-yiyu-me.git
cd src-blog-yiyu-me
git submodule init
git submodule update
npm install
```

### 写作、部署

要写一篇新博客、预览

``` bash
hexo new new-blog-name
hexo server
```

发布新的博客

```
hexo clean
hexo deploy
# also commit in the blog source
git commit -am "add a new blog"
git push origin master
```

